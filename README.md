# R_Store_sales_predictions


The data set contains historical sales data for 45 Walmart stores located in different regions. 
Each store contains a number of departments.
sales.csv 
This is the historical training data, which covers from 2/05/2010 to 11/01/2012.
Within this file you will find the following fields:
• Store - the store number
• Dept - the department number
• Date - the week
• Weekly_Sales - sales for the given department in the given store
• IsHoliday - whether the week is a special holiday week
features.csv 
This file contains additional data related to the store, department, and regional activity for the given dates. 
It contains the following fields:
• Store - the store number
• Date - the week
• Temperature - average temperature in the region
• Fuel_Price - cost of fuel in the region
• MarkDown1-5 - anonymized data related to promotional markdowns that Walmart is running. MarkDown data is only available after Nov 2011, and is not available for all stores all the time. Any missing value is marked with an NA.
• CPI - the consumer price index
• Unemployment - the unemployment rate
• IsHoliday - whether the week is a special holiday week


The goal of the project is to perform time series analysis on this dataset. Each team
is tasked with predicting the department-wide sales of one store for the next 8 weeks. Each
team can pick a store among store 1, 2, 4, 6, 8, or 11. You are expected to apply techniques
learned in this course and make forecasts based on your best model.
